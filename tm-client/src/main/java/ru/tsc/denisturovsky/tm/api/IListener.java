package ru.tsc.denisturovsky.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.denisturovsky.tm.event.ConsoleEvent;

public interface IListener {

    void execute(ConsoleEvent consoleEvent);

    @Nullable
    String getArgument();

    @NotNull
    String getDescription();

    @NotNull
    String getName();

}

