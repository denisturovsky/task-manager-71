package ru.tsc.denisturovsky.tm.api.endpoint;

import ru.tsc.denisturovsky.tm.dto.request.AbstractRequest;
import ru.tsc.denisturovsky.tm.dto.response.AbstractResponse;

@FunctionalInterface
public interface Operation<RQ extends AbstractRequest, RS extends AbstractResponse> {

    RS execute(RQ request);

}
