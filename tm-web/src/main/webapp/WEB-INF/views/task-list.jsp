<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="../include/_header.jsp"/>

<h3>TASK LIST</h3>

<table>
    <tr style="background-color: #CCE5FF;">
        <th style="width: 22%;">ID</th>
        <th style="width: 15%;">Name</th>
        <th style="width: 15%;">Description</th>
        <th style="width: 10%;">Status</th>
        <th style="width: 5%;">Created</th>
        <th style="width: 10%;">Date Begin</th>
        <th style="width: 10%;">Date End</th>
        <th style="width: 22%;">Project</th>
        <th style="width: 5%;">Edit</th>
        <th style="width: 6%;">Delete</th>
    </tr>
    <c:forEach var="task" items="${tasks}">
        <tr>
            <td>
                <c:out value="${task.id}"/>
            </td>
            <td>
                <c:out value="${task.name}"/>
            </td>
            <td>
                <c:out value="${task.description}"/>
            </td>
            <td>
                <c:out value="${task.status.displayName}"/>
            </td>
            <td>
                <fmt:formatDate pattern="dd.MM.yyyy" value="${task.created}"/>
            </td>
            <td>
                <fmt:formatDate pattern="dd.MM.yyyy" value="${task.dateBegin}"/>
            </td>
            <td>
                <fmt:formatDate pattern="dd.MM.yyyy" value="${task.dateEnd}"/>
            </td>
            <td>
                <c:out value="${projectService.findOneById(task.getProject().getId()).name}"/>
            </td>
            <td>
                <a href="/task/edit/${task.id}"/>EDIT</a>
            </td>
            <td>
                <a href="/task/delete/${task.id}"/>DELETE</a>
            </td>
        </tr>
    </c:forEach>
</table>

<form action="/task/create" style="padding-top: 20px;">
    <button>CREATE TASK</button>
</form>

<jsp:include page="../include/_footer.jsp"/>